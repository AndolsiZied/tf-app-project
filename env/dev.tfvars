name    = "infra-stack"
prefix          = "tf-interview-dev"
owners          = ["user:zandolsi@acegik.fr"]
activate_apis = [
  "cloudbilling.googleapis.com",
  "container.googleapis.com",
  "sqladmin.googleapis.com",
  "servicenetworking.googleapis.com",
]
