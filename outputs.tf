output "project_id" {
  description = "The created project ID."
  value       = module.project-factory.project_id
}